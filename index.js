// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:
const yourGrade = (grade) => {
    if (grade <= 74) {
        console.log(`Sorry, your grade is ${grade}. You have received a Failed mark.`)
    } 
    if (grade >= 75 && grade <= 80) {
        console.log(`Congratulations! Your grade is ${grade}. You have received a Beginner mark.`)
    } 
    if (grade >= 81 && grade <= 85) {
        console.log(`Congratulations! Your grade is ${grade}. You have received a Developing mark.`)
    } 
    if (grade >= 86 && grade <= 90) {
        console.log(`Congratulations! Your grade is ${grade}. You have received a Above Average mark.`)
    } 
    if (grade > 90) {
        console.log(`Congratulations! Your grade is ${grade}. You have received a Advance mark.`)
    }
};
yourGrade(81);
yourGrade(91);

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.

*/
// Code here:
for (let count = 1; count <= 300; count++){
	if (count%2 == 0) {
		console.log(`${count} is even.`)
	} else {
		console.log(`${count} is odd.`)
	};
};

/*
	3. Create a an object named ""hero"" and input the details using prompt(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/
let heroName = prompt("What is your hero name?");
let origin = prompt("Where is your origin?");
let description = prompt("Describe yourself.");
let skills = [];
for (let i = 0; i < 3; i++){
	skills[i] = (prompt(`What are your 3 unique skills? skill${i}`));
};
let hero = JSON.stringify({
	heroName: heroName,
	origin: origin,
	description: description,
	skills: skills
});
console.log(hero);
